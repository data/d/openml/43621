# OpenML dataset: BSE-30-daily-market-price-(2008-2018)

https://www.openml.org/d/43621

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
The SP BSE SENSEX (SP Bombay Stock Exchange Sensitive Index), also called the BSE 30 or simply the SENSEX, is a free-float market-weighted stock market index of 30 well-established and financially sound companies listed on Bombay Stock Exchange. The 30 component companies which are some of the largest and most actively traded stocks, are representative of various industrial sectors of the Indian economy.
This dataset contains the data about these 30 stocks for 10 years starts from 06/05/2008 to 04/05/2018 .
Variables are symbol (ticker) , Date , open , high , low , close, adj close and volume.
Acknowledgement
The prices are fetched from yahoo finance.
Inspiration
1) Which stocks were most Volatile/ Stable?
2) Predicting next day stock prices

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43621) of an [OpenML dataset](https://www.openml.org/d/43621). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43621/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43621/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43621/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

